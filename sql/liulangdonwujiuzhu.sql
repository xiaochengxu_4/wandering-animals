/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80033 (8.0.33)
 Source Host           : localhost:3306
 Source Schema         : liulangdonwujiuzhu

 Target Server Type    : MySQL
 Target Server Version : 80033 (8.0.33)
 File Encoding         : 65001

 Date: 10/03/2024 15:20:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chongwu
-- ----------------------------
DROP TABLE IF EXISTS `chongwu`;
CREATE TABLE `chongwu` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `chongwu_name` varchar(200) DEFAULT NULL COMMENT '寻宠标题  Search111 ',
  `chongwu_photo` varchar(200) DEFAULT NULL COMMENT '寻宠照片',
  `chongwu_address` varchar(200) DEFAULT NULL COMMENT '位置',
  `zan_number` int DEFAULT NULL COMMENT '赞',
  `cai_number` int DEFAULT NULL COMMENT '踩',
  `chongwu_types` int DEFAULT NULL COMMENT '寻宠类型 Search111',
  `chongwu_clicknum` int DEFAULT NULL COMMENT '寻宠热度',
  `chongwu_content` longtext COMMENT '寻宠介绍 ',
  `chongwu_delete` int DEFAULT NULL COMMENT '逻辑删除',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '录入时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间  show1 show2 photoShow',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3 COMMENT='寻宠';

-- ----------------------------
-- Records of chongwu
-- ----------------------------
BEGIN;
INSERT INTO `chongwu` (`id`, `chongwu_name`, `chongwu_photo`, `chongwu_address`, `zan_number`, `cai_number`, `chongwu_types`, `chongwu_clicknum`, `chongwu_content`, `chongwu_delete`, `insert_time`, `create_time`) VALUES (15, '悬赏寻找白色小猫咪', '/upload/1710054549560.jpeg', '在朝阳路的公园走失', 1, 1, 1, 1, '<p>我的猫咪一岁多了，白色的小猫，看起来很温顺，头上有个黑色的斑点，好记，在朝阳路公园走失，请有线索的联系我：1455485485 必有重赏</p>', 1, '2024-03-10 15:10:22', '2024-03-10 15:10:22');
COMMIT;

-- ----------------------------
-- Table structure for chongwu_collection
-- ----------------------------
DROP TABLE IF EXISTS `chongwu_collection`;
CREATE TABLE `chongwu_collection` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chongwu_id` int DEFAULT NULL COMMENT '寻宠',
  `yonghu_id` int DEFAULT NULL COMMENT '用户',
  `chongwu_collection_types` int DEFAULT NULL COMMENT '类型',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '收藏时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间 show3 photoShow',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 COMMENT='寻宠收藏';

-- ----------------------------
-- Records of chongwu_collection
-- ----------------------------
BEGIN;
INSERT INTO `chongwu_collection` (`id`, `chongwu_id`, `yonghu_id`, `chongwu_collection_types`, `insert_time`, `create_time`) VALUES (15, 10, 1, 2, '2024-05-09 11:36:08', '2024-05-09 11:36:08');
COMMIT;

-- ----------------------------
-- Table structure for chongwu_liuyan
-- ----------------------------
DROP TABLE IF EXISTS `chongwu_liuyan`;
CREATE TABLE `chongwu_liuyan` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chongwu_id` int DEFAULT NULL COMMENT '寻宠',
  `yonghu_id` int DEFAULT NULL COMMENT '用户',
  `chongwu_liuyan_text` longtext COMMENT '留言内容',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '留言时间',
  `reply_text` longtext COMMENT '回复内容',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '回复时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3 COMMENT='寻宠留言';

-- ----------------------------
-- Records of chongwu_liuyan
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) DEFAULT NULL COMMENT '配置参数名称',
  `value` varchar(100) DEFAULT NULL COMMENT '配置参数值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='配置文件';

-- ----------------------------
-- Records of config
-- ----------------------------
BEGIN;
INSERT INTO `config` (`id`, `name`, `value`) VALUES (1, '轮播图1', 'upload/config1.jpg');
INSERT INTO `config` (`id`, `name`, `value`) VALUES (2, '轮播图2', 'upload/config2.jpg');
INSERT INTO `config` (`id`, `name`, `value`) VALUES (3, '轮播图3', 'upload/config3.jpg');
COMMIT;

-- ----------------------------
-- Table structure for dictionary
-- ----------------------------
DROP TABLE IF EXISTS `dictionary`;
CREATE TABLE `dictionary` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dic_code` varchar(200) DEFAULT NULL COMMENT '字段',
  `dic_name` varchar(200) DEFAULT NULL COMMENT '字段名',
  `code_index` int DEFAULT NULL COMMENT '编码',
  `index_name` varchar(200) DEFAULT NULL COMMENT '编码名字  Search111 ',
  `super_id` int DEFAULT NULL COMMENT '父字段id',
  `beizhu` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb3 COMMENT='字典';

-- ----------------------------
-- Records of dictionary
-- ----------------------------
BEGIN;
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (1, 'chongwu_types', '寻宠类型', 1, '悬赏寻宠', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (2, 'chongwu_types', '寻宠类型', 2, '志愿寻宠', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (4, 'chongwu_collection_types', '收藏表类型', 1, '收藏', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (5, 'chongwu_collection_types', '收藏表类型', 2, '赞', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (6, 'chongwu_collection_types', '收藏表类型', 3, '踩', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (9, 'dongwu_types', '求助类型', 3, '动物找寻', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (10, 'qiuzhuzhuangtai_types', '求助状态', 1, '未完成', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (11, 'qiuzhuzhuangtai_types', '求助状态', 2, '已完成', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (12, 'dongwu_collection_types', '收藏表类型', 1, '收藏', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (13, 'dongwu_yuyue_yesno_types', '审核状态', 1, '待审核', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (14, 'dongwu_yuyue_yesno_types', '审核状态', 2, '同意', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (15, 'dongwu_yuyue_yesno_types', '审核状态', 3, '拒绝', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (17, 'news_types', '公告类型', 2, '动物科普', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (18, 'news_types', '公告类型', 3, '救助活动', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (19, 'sex_types', '性别类型', 1, '男', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (20, 'sex_types', '性别类型', 2, '女', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (21, 'forum_state_types', '帖子状态', 1, '发帖', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (22, 'forum_state_types', '帖子状态', 2, '回帖', NULL, NULL, '2024-03-10 14:59:23');
INSERT INTO `dictionary` (`id`, `dic_code`, `dic_name`, `code_index`, `index_name`, `super_id`, `beizhu`, `create_time`) VALUES (24, 'dongwu_types', '求助类型', 4, '喂养求助', NULL, '', '2024-03-10 14:59:23');
COMMIT;

-- ----------------------------
-- Table structure for dongwu
-- ----------------------------
DROP TABLE IF EXISTS `dongwu`;
CREATE TABLE `dongwu` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键 ',
  `dongwu_name` varchar(200) DEFAULT NULL COMMENT '求助名称  Search111 ',
  `dongwu_photo` varchar(200) DEFAULT NULL COMMENT '求助照片',
  `dongwu_types` int DEFAULT NULL COMMENT '求助类型 Search111',
  `yonghu_id` int DEFAULT NULL COMMENT '用户',
  `dongwu_clicknum` int DEFAULT NULL COMMENT '求助热度',
  `dongwu_content` longtext COMMENT '求助详情',
  `qiuzhuzhuangtai_types` int DEFAULT NULL COMMENT '求助状态 Search111',
  `dongwu_delete` int DEFAULT NULL COMMENT '逻辑删除',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '录入时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间  show1 show2 photoShow homeMain',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3 COMMENT='动物求助';

-- ----------------------------
-- Records of dongwu
-- ----------------------------
BEGIN;
INSERT INTO `dongwu` (`id`, `dongwu_name`, `dongwu_photo`, `dongwu_types`, `yonghu_id`, `dongwu_clicknum`, `dongwu_content`, `qiuzhuzhuangtai_types`, `dongwu_delete`, `insert_time`, `create_time`) VALUES (15, '询问一下十来斤的小狗，半岁早餐吃什么？？', '/upload/1710054386666.jpeg', 4, 3, 1, '<p>111</p>', 1, 1, '2024-03-10 15:06:40', '2024-03-10 15:06:40');
COMMIT;

-- ----------------------------
-- Table structure for dongwu_collection
-- ----------------------------
DROP TABLE IF EXISTS `dongwu_collection`;
CREATE TABLE `dongwu_collection` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dongwu_id` int DEFAULT NULL COMMENT '求助',
  `yonghu_id` int DEFAULT NULL COMMENT '用户',
  `dongwu_collection_types` int DEFAULT NULL COMMENT '类型',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '收藏时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间 show3 photoShow',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3 COMMENT='求助收藏';

-- ----------------------------
-- Records of dongwu_collection
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for dongwu_liuyan
-- ----------------------------
DROP TABLE IF EXISTS `dongwu_liuyan`;
CREATE TABLE `dongwu_liuyan` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dongwu_id` int DEFAULT NULL COMMENT '求助',
  `yonghu_id` int DEFAULT NULL COMMENT '用户',
  `dongwu_liuyan_text` longtext COMMENT '留言内容',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '留言时间',
  `reply_text` longtext COMMENT '回复内容',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '回复时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3 COMMENT='求助留言';

-- ----------------------------
-- Records of dongwu_liuyan
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for dongwu_yuyue
-- ----------------------------
DROP TABLE IF EXISTS `dongwu_yuyue`;
CREATE TABLE `dongwu_yuyue` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dongwu_id` int DEFAULT NULL COMMENT '求助',
  `yonghu_id` int DEFAULT NULL COMMENT '用户',
  `dongwu_yuyue_text` longtext COMMENT '领养缘由',
  `dongwu_yuyue_yesno_types` int DEFAULT NULL COMMENT '审核状态 Search111 ',
  `dongwu_yuyue_yesno_text` longtext COMMENT '审核回复',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '申请时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间 show3 listShow',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3 COMMENT='求助报名';

-- ----------------------------
-- Records of dongwu_yuyue
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for forum
-- ----------------------------
DROP TABLE IF EXISTS `forum`;
CREATE TABLE `forum` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `forum_name` varchar(200) DEFAULT NULL COMMENT '帖子标题  Search111 ',
  `yonghu_id` int DEFAULT NULL COMMENT '用户',
  `users_id` int DEFAULT NULL COMMENT '管理员',
  `forum_content` longtext COMMENT '发布内容',
  `super_ids` int DEFAULT NULL COMMENT '父id',
  `forum_state_types` int DEFAULT NULL COMMENT '帖子状态',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '发帖时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间 show2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3 COMMENT='论坛';

-- ----------------------------
-- Records of forum
-- ----------------------------
BEGIN;
INSERT INTO `forum` (`id`, `forum_name`, `yonghu_id`, `users_id`, `forum_content`, `super_ids`, `forum_state_types`, `insert_time`, `update_time`, `create_time`) VALUES (5, '示例帖子1', 2, NULL, '<p>发布内容…………………………………………</p>', 460, 1, '2024-03-10 15:07:57', '2024-03-10 15:07:57', '2024-03-10 15:07:57');
COMMIT;

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `news_name` varchar(200) DEFAULT NULL COMMENT '公告标题  Search111 ',
  `news_types` int DEFAULT NULL COMMENT '公告类型  Search111 ',
  `news_photo` varchar(200) DEFAULT NULL COMMENT '公告图片',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '添加时间',
  `news_content` longtext COMMENT '公告详情',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间 show1 show2 nameShow',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3 COMMENT='公告通知';

-- ----------------------------
-- Records of news
-- ----------------------------
BEGIN;
INSERT INTO `news` (`id`, `news_name`, `news_types`, `news_photo`, `insert_time`, `news_content`, `create_time`) VALUES (15, '动物园救助猫咪', 3, '/upload/1710054772328.jpeg', '2024-03-10 15:12:56', '<p>最近有一场救助流浪猫的活动，原因是小猫咪进入了动物园中，与猴子在一块，猴子们一直在欺负它，我们要组织一批志愿者去拯救猫咪……</p>', '2024-03-10 15:12:56');
COMMIT;

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userid` bigint NOT NULL COMMENT '管理id',
  `username` varchar(100) NOT NULL COMMENT '管理名',
  `tablename` varchar(100) DEFAULT NULL COMMENT '表名',
  `role` varchar(100) DEFAULT NULL COMMENT '角色',
  `token` varchar(200) NOT NULL COMMENT '密码',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '新增时间',
  `expiratedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='token表';

-- ----------------------------
-- Records of token
-- ----------------------------
BEGIN;
INSERT INTO `token` (`id`, `userid`, `username`, `tablename`, `role`, `token`, `addtime`, `expiratedtime`) VALUES (1, 1, 'admin', 'users', '管理员', 'oobynfe3rgpyykjwkfxedo32tj75n69m', '2024-03-10 15:27:47', '2024-03-10 15:27:47');
INSERT INTO `token` (`id`, `userid`, `username`, `tablename`, `role`, `token`, `addtime`, `expiratedtime`) VALUES (2, 1, 'a1', 'yonghu', '用户', '8v4qk4u9qhjlpk2nfy5l1942uw4u2b9h', '2024-03-10 15:27:47', '2024-03-10 15:27:47');
INSERT INTO `token` (`id`, `userid`, `username`, `tablename`, `role`, `token`, `addtime`, `expiratedtime`) VALUES (3, 2, 'a2', 'yonghu', '用户', 'apjjhpcbp5ir28bufkp1x2d6e3x71nv5', '2024-03-10 15:27:47', '2024-03-10 15:27:47');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(100) NOT NULL COMMENT '医院名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `role` varchar(100) DEFAULT '管理员' COMMENT '角色',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '新增时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='管理员';

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` (`id`, `username`, `password`, `role`, `addtime`) VALUES (1, 'admin', 'admin', '管理员', '2024-05-09 09:56:30');
COMMIT;

-- ----------------------------
-- Table structure for yonghu
-- ----------------------------
DROP TABLE IF EXISTS `yonghu`;
CREATE TABLE `yonghu` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(200) DEFAULT NULL COMMENT '账户',
  `password` varchar(200) DEFAULT NULL COMMENT '密码',
  `yonghu_name` varchar(200) DEFAULT NULL COMMENT '用户名称 Search111 ',
  `yonghu_phone` varchar(200) DEFAULT NULL COMMENT '用户手机号',
  `yonghu_id_number` varchar(200) DEFAULT NULL COMMENT '用户身份证号',
  `yonghu_photo` varchar(200) DEFAULT NULL COMMENT '用户头像',
  `sex_types` int DEFAULT NULL COMMENT '性别 Search111',
  `yonghu_email` varchar(200) DEFAULT NULL COMMENT '用户邮箱',
  `yonghu_delete` int DEFAULT NULL COMMENT '逻辑删除',
  `insert_time` timestamp NULL DEFAULT NULL COMMENT '添加时间',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='用户';

-- ----------------------------
-- Records of yonghu
-- ----------------------------
BEGIN;
INSERT INTO `yonghu` (`id`, `username`, `password`, `yonghu_name`, `yonghu_phone`, `yonghu_id_number`, `yonghu_photo`, `sex_types`, `yonghu_email`, `yonghu_delete`, `insert_time`, `create_time`) VALUES (2, 'user2', '123456', '王五', '17703786902', '410224199010102002', 'upload/yonghu2.jpg', 2, '2@qq.com', 1, '2024-05-09 09:56:50', '2024-05-09 09:56:50');
INSERT INTO `yonghu` (`id`, `username`, `password`, `yonghu_name`, `yonghu_phone`, `yonghu_id_number`, `yonghu_photo`, `sex_types`, `yonghu_email`, `yonghu_delete`, `insert_time`, `create_time`) VALUES (3, 'user1', '123456', '张三', '17703786903', '410224199010102003', '/upload/1710054292058.jpeg', 2, '3@qq.com', 1, '2024-05-09 09:56:50', '2024-05-09 09:56:50');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
