package com.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.entity.JuankuanEntity;
import com.entity.ZhiyuanzheEntity;
import com.entity.view.JuankuanView;
import com.entity.view.ZhiyuanzheView;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 动物求助 Dao 接口
 *
 * @author
 */
public interface ZhiyuanzheDao extends BaseMapper<ZhiyuanzheEntity> {

   List<ZhiyuanzheView> selectListView(@Param("params")Map<String,Object> params);

}
