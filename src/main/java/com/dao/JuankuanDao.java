package com.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.entity.DongwuEntity;
import com.entity.JuankuanEntity;
import com.entity.view.DongwuView;
import com.entity.view.JuankuanView;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 动物求助 Dao 接口
 *
 * @author
 */
public interface JuankuanDao extends BaseMapper<JuankuanEntity> {

   List<JuankuanView> selectListView(@Param("params")Map<String,Object> params);

}
