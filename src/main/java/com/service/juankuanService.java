package com.service;

import com.baomidou.mybatisplus.service.IService;
import com.entity.JuankuanEntity;
import com.entity.view.JuankuanView;
import com.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 动物求助 服务类
 */
public interface juankuanService extends IService<JuankuanEntity> {

    /**
    * @param params 查询参数
    * @return 带分页的查询出来的数据
    */
     List<JuankuanView> queryPage(Map<String, Object> params);

}
