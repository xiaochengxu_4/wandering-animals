package com.service;

import com.baomidou.mybatisplus.service.IService;
import com.entity.JuankuanEntity;
import com.entity.ZhiyuanzheEntity;
import com.entity.view.JuankuanView;
import com.entity.view.ZhiyuanzheView;

import java.util.List;
import java.util.Map;

/**
 * 动物求助 服务类
 */
public interface ZhiyuanzheService extends IService<ZhiyuanzheEntity> {

    /**
    * @param params 查询参数
    * @return 带分页的查询出来的数据
    */
     List<ZhiyuanzheView> queryPage(Map<String, Object> params);

}
