package com.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.dao.DongwuDao;
import com.dao.JuankuanDao;
import com.entity.DongwuEntity;
import com.entity.JuankuanEntity;
import com.entity.view.DongwuView;
import com.entity.view.JuankuanView;
import com.service.DongwuService;
import com.service.juankuanService;
import com.utils.PageUtils;
import com.utils.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 动物求助 服务实现类
 */
@Service("juankuanService")
@Transactional
public class juankuanServiceImpl extends ServiceImpl<JuankuanDao, JuankuanEntity> implements juankuanService {

    @Override
    public List<JuankuanView> queryPage(Map<String,Object> params) {
//        List<JuankuanView> page =new Query<JuankuanView>(params).getPage();
        List<JuankuanView> list= baseMapper.selectListView(params);
        return list;
    }

}
