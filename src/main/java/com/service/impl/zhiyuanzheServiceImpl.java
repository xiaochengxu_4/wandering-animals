package com.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.dao.ZhiyuanzheDao;
import com.entity.JuankuanEntity;
import com.entity.ZhiyuanzheEntity;
import com.entity.view.ZhiyuanzheView;
import com.service.ZhiyuanzheService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 动物求助 服务实现类
 */
@Service("zhiyuanzheService")
@Transactional
public class zhiyuanzheServiceImpl extends ServiceImpl<ZhiyuanzheDao, ZhiyuanzheEntity> implements ZhiyuanzheService {

    @Override
    public List<ZhiyuanzheView> queryPage(Map<String,Object> params) {
        List<ZhiyuanzheView> list= baseMapper.selectListView(params);
        return list;
    }

}
