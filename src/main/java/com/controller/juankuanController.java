
package com.controller;

import com.alibaba.fastjson.JSONObject;
import com.annotation.IgnoreAuth;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.entity.DongwuEntity;
import com.entity.JuankuanEntity;
import com.entity.YonghuEntity;
import com.entity.view.DongwuCollectionView;
import com.entity.view.DongwuView;
import com.entity.view.JuankuanView;
import com.service.*;
import com.utils.CommonUtil;
import com.utils.PageUtils;
import com.utils.PoiUtil;
import com.utils.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 捐款
 * 后端接口
 * @author
 * @email
*/
@RestController
@Controller
@RequestMapping("/juankuan")
public class juankuanController {
    @Autowired
    private juankuanService juankuan1Service;



    /**
    * 后端列表
    */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params, HttpServletRequest request){
        List<JuankuanView> page = juankuan1Service.queryPage(params);
        //查询对应帖子的捐款数额 然后统计起来



        return R.ok().put("data", page);
    }

    /**
    * 后端保存
    */
    @RequestMapping("/save")
    public R save(@RequestBody JuankuanEntity dongwu, HttpServletRequest request){
            //获取当前登陆人的id 和 姓名
            dongwu.setCreatTime(new Date());
            juankuan1Service.insert(dongwu);
            return R.ok();

    }


}

