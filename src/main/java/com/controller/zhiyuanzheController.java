
package com.controller;

import com.entity.JuankuanEntity;
import com.entity.ZhiyuanzheEntity;
import com.entity.view.JuankuanView;
import com.entity.view.ZhiyuanzheView;
import com.service.ZhiyuanzheService;
import com.service.juankuanService;
import com.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 志愿者
 * 后端接口
 * @author
 * @email
*/
@RestController
@Controller
@RequestMapping("/zhiyuanzhe")
public class zhiyuanzheController {
    @Autowired
    private ZhiyuanzheService zhiyuanzheService;



    /**
    * 后端列表
    */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params, HttpServletRequest request){
        List<ZhiyuanzheView> page = zhiyuanzheService.queryPage(params);
        return R.ok().put("data", page);
    }

    /**
    * 后端保存
    */
    @RequestMapping("/save")
    public R save(@RequestBody ZhiyuanzheEntity dongwu, HttpServletRequest request){
            //获取当前登陆人的id 和 姓名
            dongwu.setCreatTime(new Date());
        zhiyuanzheService.insert(dongwu);
            return R.ok();

    }


}

