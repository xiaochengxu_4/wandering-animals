package com.entity;

import com.annotation.ColumnInfo;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 *
 *
 * @author
 * @email
 */
@TableName("zhiyuanzhe")
@Data
public class ZhiyuanzheEntity<T> implements Serializable {


    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    @ColumnInfo(comment="主键",type="int(11)")
    @TableField(value = "id")

    private Integer id;


    /**
     * 求助名称
     */
    @ColumnInfo(comment="求助名称",type="varchar(200)")
    @TableField(value = "jiuzhuId")

    private String jiuzhuId;


    /**
     * 求助照片
     */
    @ColumnInfo(comment="求助照片",type="varchar(200)")
    @TableField(value = "jiuzhuName")

    private String jiuzhuName;


    /**
     * 求助类型
     */
    @ColumnInfo(comment="求助类型",type="int(11)")
    @TableField(value = "zhiyuanId")

    private String zhiyuanId;


    /**
     * 用户
     */
    @ColumnInfo(comment="用户",type="int(11)")
    @TableField(value = "zhiyuanName")

    private String zhiyuanName;


    /**
     * 录入时间
     */
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat
    @ColumnInfo(comment="录入时间",type="timestamp")
    @TableField(value = "creatTime",fill = FieldFill.INSERT)

    private Date creatTime;
}
