package com.entity.view;

import com.annotation.ColumnInfo;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.entity.JuankuanEntity;
import com.entity.ZhiyuanzheEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


@TableName("zhiyuanzhe")
@Data
public class ZhiyuanzheView extends ZhiyuanzheEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	//当前表
	/**
	* 求助类型的值
	*/
	@ColumnInfo(comment="求助类型的字典表值",type="varchar(200)")
	private String dongwuValue;
	/**
	* 求助状态的值
	*/
	@ColumnInfo(comment="求助状态的字典表值",type="varchar(200)")
	private String qiuzhuzhuangtaiValue;

	//级联表 用户
		/**
		* 用户名称
		*/

		@ColumnInfo(comment="用户名称",type="varchar(200)")
		private String yonghuName;
		/**
		* 用户手机号
		*/

		@ColumnInfo(comment="用户手机号",type="varchar(200)")
		private String yonghuPhone;
		/**
		* 用户身份证号
		*/

		@ColumnInfo(comment="用户身份证号",type="varchar(200)")
		private String yonghuIdNumber;
		/**
		* 用户头像
		*/

		@ColumnInfo(comment="用户头像",type="varchar(200)")
		private String yonghuPhoto;
		/**
		* 用户邮箱
		*/

		@ColumnInfo(comment="用户邮箱",type="varchar(200)")
		private String yonghuEmail;
		/**
		* 逻辑删除
		*/

		@ColumnInfo(comment="逻辑删除",type="int(11)")
		private Integer yonghuDelete;


	/**
	 * 主键
	 */
	@TableId(type = IdType.AUTO)
	@ColumnInfo(comment="主键",type="int(11)")
	private Integer id;


	/**
	 * 求助名称
	 */
	@ColumnInfo(comment="求助名称",type="varchar(200)")
	private String jiuzhuId;


	/**
	 * 求助照片
	 */
	@ColumnInfo(comment="求助照片",type="varchar(200)")
	private String jiuzhuName;


	/**
	 * 求助类型
	 */
	@ColumnInfo(comment="求助类型",type="int(11)")
	private String zhiyuanId;


	/**
	 * 用户
	 */
	@ColumnInfo(comment="用户",type="int(11)")
	private String zhiyuanName;


	/**
	 * 录入时间
	 */
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat
	@ColumnInfo(comment="录入时间",type="timestamp")
	private Date creatTime;
}
